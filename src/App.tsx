import React, { useState } from 'react'
import './App.css'
import useProduct from './hook/useProduct';
import InfiniteScroll from "react-infinite-scroll-component";
import {CircularProgress} from '@mui/material'

function App() {
  const [query, setQuery] = React.useState("");
  const deferredQuery = React.useDeferredValue<string>(query);
  const [skip,setSkip] = useState<number>(0);
  const [isLoading, error, total,data] = useProduct({q:deferredQuery, skip: String(skip)});
  console.log(total, data.length)
  return (
    <>  
      <label htmlFor='productName'>Type here: </label>
      <input type='text' name='productName' onChange={(event) => {
        setQuery(event.target.value)
        setSkip(0)
      }}/>

        <div id='tableContent' style={{"width": "40%", "marginTop":"1rem", overflow: "auto", height: "30rem"}}>
          <InfiniteScroll
            dataLength={data.length}
            next={() => setSkip(prev => {
              prev+=20
              return prev
            })}
            hasMore={data.length === total ? false : true}
            scrollableTarget="tableContent"
            loader={<CircularProgress/>}
            endMessage="End of data"

          >
            {data.map( ({id,brand,category,price},index) => (
              <div style={{"border": "1px solid green", "margin":6,"padding":8,"height":30}} key={index}>
                {`${id} - ${brand} - ${category} - ${price}`}
              </div>
            ))}
          </InfiniteScroll>
        </div>
    
    </>
  )
}

export default App
