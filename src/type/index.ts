export interface Product {
    id: number,
    title: string,
    description: string,
    price: number,
    discountPercentage: number,
    rating: number,
    stock: number,
    brand: string,
    category: string,
    thumbnail: string
    images: string[]
}


export interface IData {
    products: Product[],
    total: number,
    skip: number,
    limit: number
}

export interface QueryParams {
    q: string,
    limit?: string,
    skip: string
}