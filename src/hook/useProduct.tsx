import React, { useRef, useState } from 'react'
import type { IData, Product, QueryParams } from '../type'
import axios from 'axios';

const GET_URL = 'https://dummyjson.com/products?limit=20'
const QUERY_URL = 'https://dummyjson.com/products/search?'

const useProduct = ({
    q = "",
    skip = "0",
    limit = "20"
}: QueryParams) => {
    const [data,setData] = React.useState<Product[]>([])
    const [isLoading, setIsloading] = useState<boolean>(false)
    const total = useRef<number>(0)
    const [error, setError] = useState<string>("")
    const isFirstRender = useRef<boolean>(true)

    React.useEffect(() => {
        const fetchProduct = async ({q,limit,skip}:QueryParams) => {
            setIsloading(true)
            try {

                if(isFirstRender.current){
                    var data = await axios.get<IData>(GET_URL)
                    const {products, total: _total} = data.data
                    isFirstRender.current = false
                    total.current = _total
                    setData(products)
                }
                else{
                    var data = await axios.get<IData>(`${QUERY_URL}q=${q}&limit=${limit}&skip=${skip}`)     
                    const {products, total: _total} = data.data
                    if((q && skip === "0") || (!q && skip ==="0")){
                        total.current = _total
                        return setData(products)
                    }
                    return setData((prevData) =>  [...prevData, ...data.data.products])
                }
                
            } catch (error) {
                setError(error as string)
                setIsloading(false)
                
            }finally{
                setIsloading(false)
            }
        }
        fetchProduct({q,limit,skip})
    },[q,skip])

    return [isLoading,error, total.current,data] as const
  
}

export default useProduct